﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizador_csharp.Utilerias
{
    class ListaRegistros
    {
        private string ruta;

        public string Ruta
        {
            get { return ruta; }
            set { ruta = value; }
        }

        private int noLinea;

        public int NoLinea
        {
            get { return noLinea; }
            set { noLinea = value; }
        }

        private string lineaCodigo;

        public string LineaCodigo
        {
            get { return lineaCodigo; }
            set { lineaCodigo = value; }
        }

        private string extension;

        public string Extension
        {
            get { return extension; }
            set { extension = value; }
        }

        private int idObjetoBD;

        public int IdObjetoBD
        {
            get { return idObjetoBD; }
            set { idObjetoBD = value; }
        }

        private string objetoPadre;

        public string ObjetoPadre
        {
            get { return objetoPadre; }
            set { objetoPadre = value; }
        }

        private int analizador;

        public int  Analizador
        {
            get { return analizador; }
            set { analizador = value; }
        }

        public ListaRegistros(string Ruta, int NoLinea, string LineaCodigo, string Extension, int IdObjetoBD, string ObjetoPadre, int Analizador)
        {
            ruta = Ruta;
            noLinea = NoLinea;
            lineaCodigo = LineaCodigo;
            extension = Extension;
            idObjetoBD = IdObjetoBD;
            objetoPadre = ObjetoPadre;
            analizador = Analizador;
        }
    }
}
