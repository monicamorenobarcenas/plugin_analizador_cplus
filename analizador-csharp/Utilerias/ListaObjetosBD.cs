﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizador_csharp.Utilerias
{
    class ListaObjetosBD
    {
        private int idObjetoBD;

        public int IdObjetoBD
        {
            get { return idObjetoBD; }
            set { idObjetoBD = value; }
        }

        private string nombreObjetoBD;

        public string NombreObjetoBD
        {
            get { return nombreObjetoBD; }
            set { nombreObjetoBD = value; }
        }

        private int idTipoObjeto;

        public int IdTipoObjeto
        {
            get { return idTipoObjeto; }
            set { idTipoObjeto = value; }
        }


        public ListaObjetosBD(int IdObjetoBD, string NombreObjetoBD, int IdTipoObjeto)
        {
            idObjetoBD = IdObjetoBD;
            nombreObjetoBD = NombreObjetoBD;
            idTipoObjeto = IdTipoObjeto;
        }
    }
}
