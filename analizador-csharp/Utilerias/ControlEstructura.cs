﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizador_csharp.Utilerias
{
    class ControlEstructura
    {
        private int nivel;

        public int Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public ControlEstructura(int Nivel, string Nombre)
        {
            nivel = Nivel;
            nombre = Nombre;
        }
    }
}
