﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizador_csharp.Utilerias
{
    class ListaEstructura
    {
        private string ruta;

        public string Ruta
        {
            get { return ruta; }
            set { ruta = value; }
        }

        private int noLinea;

        public int NoLinea
        {
            get { return noLinea; }
            set { noLinea = value; }
        }

        private string lineaCodigo;

        public string LineaCodigo
        {
            get { return lineaCodigo; }
            set { lineaCodigo = value; }
        }

        private string extension;

        public string Extension
        {
            get { return extension; }
            set { extension = value; }
        }

        private string archivo;

        public string Archivo
        {
            get { return archivo; }
            set { archivo = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string tipo;

        public string Tipo  
        {
            get { return tipo; }
            set { tipo = value; }
        }

        private string padre;

        public string Padre
        {
            get { return padre; }
            set { padre = value; }
        }

        private string nombreSecundario;

        public string NombreSecundario
        {
            get { return nombreSecundario; }
            set { nombreSecundario = value; }
        }

        public ListaEstructura(string Ruta, int NoLinea, string LineaCodigo, string Extension, string Archivo, string Nombre, string Tipo, 
            string Padre, string NombreSecundario)
        {
            ruta = Ruta;
            noLinea = NoLinea;
            lineaCodigo = LineaCodigo;
            extension = Extension;
            archivo = Archivo;
            nombre = Nombre;
            tipo = Tipo;
            padre = Padre;
            nombreSecundario = NombreSecundario;
        }
    }
}
