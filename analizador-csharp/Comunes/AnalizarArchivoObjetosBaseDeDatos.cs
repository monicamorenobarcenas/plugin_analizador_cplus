﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using analizador_csharp.Utilerias;
using analizador_csharp.LogFisico;
using analizador_csharp.Datos;

namespace analizador_csharp.Comunes
{
    class AnalizarArchivoObjetosBaseDeDatos : Log
    {
        // Recuperar la lista de patrones a buscar, compararlos contra cada línea de código y recuperar la lista de coincidencias
        public List<ListaRegistros> LeerArchivo(int AplicacionID, int AnalizadorID, HashSet<string> ListaArchivos, string Cadena)
        {
            var listaRegistros = new List<ListaRegistros>();

            try
            {
                var listaObjetosBD = new List<ListaObjetosBD>();
                var objetosBD = new ObjetosBD();
                var compararPatrones = new CompararPatrones();
                var comentarios = new Comentarios();

                // Recuperar lista de Objetos de Bases de Datos que sirve como patrones a buscar
                if (objetosBD.ObtenerCatalogoObjetos(AplicacionID, Cadena))
                {
                    var xmlObjetos = objetosBD.ResultadoXML;

                    foreach (XmlNode xmlNode in xmlObjetos.DocumentElement.SelectSingleNode("ObjetosBD").SelectNodes("row"))
                        listaObjetosBD.Add(new ListaObjetosBD(
                            int.Parse(xmlNode.Attributes["Id_ObjBD"].Value),
                            xmlNode.Attributes["Nombre"].Value,
                            int.Parse(xmlNode.Attributes["id_TipoObjBD"].Value)));
                }

                // Comparar los patrones contra las líneas de código
                foreach (string archivo in ListaArchivos)
                {
                    int noLinea = 0;
                    var fileStream = new FileStream(archivo, FileMode.Open, FileAccess.Read);

                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        string lineaCodigo;
                        while ((lineaCodigo = streamReader.ReadLine()) != null)
                        {
                            ++noLinea;

                            if (!string.IsNullOrEmpty(lineaCodigo.Trim()))
                            {
                                if (!comentarios.EsComentario(lineaCodigo))
                                {
                                    if (listaObjetosBD.Count > 0)
                                    {
                                        int idObjetoEncontrado = compararPatrones.CompararCatalogo(lineaCodigo, listaObjetosBD);

                                        // Recuperar lista de coincidencias para el analizador de base de datos
                                        if (idObjetoEncontrado > 0)
                                            listaRegistros.Add(new ListaRegistros(
                                                archivo,
                                                noLinea,
                                                lineaCodigo,
                                                Regex.Replace(Path.GetExtension(archivo), @"\.", string.Empty).Trim(),
                                                idObjetoEncontrado,
                                                Regex.Replace(Path.GetFileName(archivo), @"\.\w+", string.Empty).Trim(),
                                                AnalizadorID));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR AnalizarArchivo.LeerArchivo " + ex.Message.ToString());
            }

            return listaRegistros;
        }
    }
}