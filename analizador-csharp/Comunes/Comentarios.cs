﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizador_csharp.Comunes
{
    class Comentarios
    {
        public bool EsComentario(string LineaCodigo)
        {
            bool resultado = false;

            string lineaCodigo = LineaCodigo.ToLower();

            if (lineaCodigo.StartsWith("//") || lineaCodigo.StartsWith("/*") || lineaCodigo.StartsWith("*/") ||
                lineaCodigo.StartsWith("<!--") || lineaCodigo.StartsWith("'"))
            {
                resultado = true;
            }
            return resultado;
        }
    }
}
