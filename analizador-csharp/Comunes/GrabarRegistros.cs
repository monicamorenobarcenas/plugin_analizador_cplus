﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using analizador_csharp.Datos;
using analizador_csharp.Utilerias;
using analizador_csharp.LogFisico;

namespace analizador_csharp.Comunes
{
    class GrabarRegistros : Log
    {
        // Ejecuta el procedimiento para insertar la lista de coincidencias en la base de datos
        public bool GuardarListaRegistros(int ProcesoID, int AplicacionID, int AnalizadorID, List<ListaRegistros> ListaRegistros, string Cadena)
        {
            bool respuesta = false;

            try
            {
                var procesos = new Procesos();

                switch(AnalizadorID)
                {
                    // Analizadores Comunes
                    case 1: case 2: case 3: case 4: case 5: case 7:
                        respuesta = procesos.GuardarRegistrosAnalizadoresComunes(ProcesoID, AplicacionID, ListaRegistros, Cadena);
                        break;

                    // Analizador Objetos Base de Datos
                    case 6:
                        respuesta = procesos.GuardarRegistrosProcesoBaseDeDatos(ProcesoID, AplicacionID, ListaRegistros, Cadena);
                        break;
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR GrabarRegistros.GuardarListaRegistros " + ex.Message.ToString());
            }

            return respuesta;
        }

        // Ejecuta el procedimiento para insertar la lista de coincidencias en la base de datos
        public bool GuardarListaRegistros(int ProcesoID, int AplicacionID, int AnalizadorID, List<ListaEstructura> ListaRegistros, string Cadena)
        {
            bool respuesta = false;

            try
            {
                var procesos = new Procesos();

                respuesta = procesos.GuardarRegistrosProcesoEstructura(ProcesoID, AplicacionID, ListaRegistros, Cadena);

            }
            catch (Exception ex)
            {
                EscribeLog("ERROR GrabarRegistros.GuardarListaRegistros " + ex.Message.ToString());
            }

            return respuesta;
        }
    }
}
