﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using analizador_csharp.Datos;
using analizador_csharp.LogFisico;

namespace analizador_csharp.Comunes
{
    class AdministrarProcesos : Log
    {
        // Ejecuta el procedimiento almacenado que recuperar el número de proceso siguiente
        public int ObtenerProceso(string Cadena)
        {
            int nuevoProceso = 0;

            try
            {
                var procesos = new Procesos();

                if (procesos.ObtenerProceso(Cadena))
                {
                    var xmlProcesos = procesos.ResultadoXML;

                    foreach (XmlNode xmlNode in xmlProcesos.DocumentElement.SelectSingleNode("Procesos").SelectNodes("row"))
                    {
                        nuevoProceso = int.Parse(xmlNode.Attributes["Siguiente"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR AdministrarProcesos.ObtenerProceso " + ex.Message.ToString());
            }
            
            return nuevoProceso;
        }

        // Ejecuta el procedimiento  que inserta el registro correspondiente al Proceso en la base de datos con estatus inicial
        public bool GenerarProceso(int ProcesoID, int UsuarioID, int AplicacionID, int AnalizadorID, string Cadena)
        {
            bool respuesta = false;

            try
            {
                var procesos = new Procesos();

                if (procesos.GenerarProceso(ProcesoID, UsuarioID, AplicacionID, AnalizadorID, Cadena))
                    respuesta = true;
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR AdministrarProcesos.GenerarProceso " + ex.Message.ToString());
            }

            return respuesta;
        }

        // Ejecuta el procedimiento  que actualiza el registro correspondiente al Proceso en la base de datos con estatus inicial
        public bool ActualizaProceso(int ProcesoID, int AnalizadorID, int EstatusID, string Cadena)
        {
            bool respuesta = false;

            try
            {
                var procesos = new Procesos();

                if (procesos.ActualizaEstatusProceso(ProcesoID, AnalizadorID, EstatusID, Cadena))
                    respuesta = true;
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR AdministrarProcesos.ActualizaProceso " + ex.Message.ToString());
            }

            return respuesta;
        }
    }
}
