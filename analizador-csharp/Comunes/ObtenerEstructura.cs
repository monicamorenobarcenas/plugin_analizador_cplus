﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using analizador_csharp.Utilerias;
using analizador_csharp.LogFisico;
using analizador_csharp.Datos;

namespace analizador_csharp.Comunes
{
    class ObtenerEstructura : Log
    {

        int controlNivel;
        List<ControlEstructura> listaPadres = new List<ControlEstructura>();
        public string Nombre = "", Tipo = "", Padre = "";

        public bool ListaEstructura(string Ruta, int NoLinea, string LineaCodigo, string UltimoNombreValido,
            bool Inventario, HashSet<ControlEstructura> ListaInventario)
        {
            bool resultado = false;

            try
            {
                //Identificar que la Línea de Código tiene include
                Regex regex = new Regex(@"(#include)");
                Match match = regex.Match(LineaCodigo.Trim());

                if (match.Success)
                {
                    Nombre = Regex.Replace(Regex.Replace(LineaCodigo, @"(#include)\s*", string.Empty), ";", string.Empty).Trim();
                    Tipo = "Directivas";
                    Padre = Regex.Replace(Path.GetFileName(Ruta), @"\.\w+", string.Empty).Trim();
                    return resultado = true;
                }

                //Identificar los Define que se utilizan
                regex = new Regex(@"(#define)");
                match = regex.Match(LineaCodigo.Trim());

                if (match.Success)
                {
                    Nombre = Regex.Replace(LineaCodigo, @"(#define)\s*", string.Empty).Trim();
                    Tipo = "Directivas";
                    Padre = listaPadres.Count == 0 ?
                        Regex.Replace(Path.GetFileName(Ruta), @"\.\w+", string.Empty).Trim() :
                        listaPadres.Last().Nombre;
                    return resultado = true;
                }

                //Identificar que la Línea de Código es Clase
                regex = new Regex(@"(class)\s*\w+");
                match = regex.Match(LineaCodigo.Trim());

                if (match.Success)
                {
                    Nombre = Regex.Replace(match.Value, @"(class)\s*", string.Empty).Trim();
                    Tipo = "Clase";
                    Padre = listaPadres.Count == 0 ?
                        Regex.Replace(Path.GetFileName(Ruta), @"\.\w+", string.Empty).Trim() :
                        listaPadres.Last().Nombre;
                    return resultado = true;
                }

                //Identificar que la Línea de Código es Constructor
                if (listaPadres.Count > 0)
                {
                    regex = new Regex(@"(" + listaPadres.Last().Nombre + @")(\s*\(|\()");
                    match = regex.Match(LineaCodigo.Trim());

                    if (match.Success)
                    {
                        regex = new Regex(@"((\w+|\w+\s*)\()");
                        match = regex.Match(LineaCodigo.Trim());

                        if (match.Success)
                        {
                            Nombre = Regex.Replace(match.Value, @"(\()", string.Empty).Trim();
                            Tipo = "Constructor";
                            Padre = listaPadres.Count == 0 ?
                                Regex.Replace(Path.GetFileName(Ruta), @"\.\w+", string.Empty).Trim() :
                                listaPadres.Last().Nombre;
                            return resultado = true;
                        }
                    }
                }

                //Identificar que la Línea de Código es Método
                regex = new Regex(@"\b(public|private|protected|void|^bool|^int|^Struct)\b\s*\w+");
                match = regex.Match(LineaCodigo.Trim());

                if (match.Success)
                {
                    regex = new Regex(@"((\w+|\w+\s*)\()");
                    match = regex.Match(LineaCodigo.Trim());

                    if (match.Success)
                    {
                        Nombre = Regex.Replace(match.Value, @"(\()", string.Empty).Trim();
                        Tipo = "Método";
                        Padre = listaPadres.Count == 0 ?
                            Regex.Replace(Path.GetFileName(Ruta), @"\.\w+", string.Empty).Trim() :
                            listaPadres.Last().Nombre;
                        return resultado = true;
                    }
                }

                //Identificar que la Línea de Código es Instancia; solo cuando no se está generando el inventario
                if (!Inventario)
                {
                    foreach (var registro in ListaInventario)
                    {
                        regex = new Regex(@"\b" + registro.Nombre + @"\b(\s*\(|\()");
                        match = regex.Match(LineaCodigo.Trim());

                        if (match.Success)
                        {
                            Nombre = Regex.Replace(match.Value, @"(\()", string.Empty).Trim();
                            Tipo = "Instancia";
                            Padre = listaPadres.Count == 0 ?
                                Regex.Replace(Path.GetFileName(Ruta), @"\.\w+", string.Empty).Trim() :
                                listaPadres.Last().Nombre;
                            return resultado = true;
                        }
                    }
                }

                //Generar la lista de elementos que son padres de otros elementos; solo cuando no se está generando el inventario
                if (!Inventario)
                    AcumularNivel(LineaCodigo, UltimoNombreValido);
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR ObtenerEstructura.ListaEstructura " + ex.Message.ToString());
            }

            return resultado;
        }

        private void AcumularNivel(string LineaCodigo, string UltimoNombre)
        {
            try
            {
                foreach (char letra in LineaCodigo.Trim())
                {
                    if (letra == '{')
                    {
                        controlNivel++;
                        listaPadres.Add(new ControlEstructura(controlNivel, UltimoNombre));
                    }

                    if (letra == '}')
                    {
                        if (listaPadres.Last().Nivel == controlNivel)
                            listaPadres.RemoveAt(listaPadres.Count - 1);
                        controlNivel--;
                    }
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR ObtenerEstructura.AcumularNivel " + ex.Message.ToString());
            }
        }

        public string NombreAplicacion(int AplicacionID, string Cadena)
        {
            var nombreAplicacion = "";
            var procesos = new Procesos();

            if (procesos.RecuperarAplicacion(AplicacionID, Cadena))
            {
                var xmlAplicacion = procesos.ResultadoXML;

                foreach (XmlNode xmlNode in xmlAplicacion.DocumentElement.SelectSingleNode("Procesos").SelectNodes("row"))
                    nombreAplicacion = xmlNode.Attributes["NombreAplicacion"].Value;
            }
            return nombreAplicacion;
        }
    }
}
