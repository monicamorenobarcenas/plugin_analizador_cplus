﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;
using analizador_csharp.Utilerias;
using analizador_csharp.LogFisico;

namespace analizador_csharp.Comunes
{
    class CompararPatrones : Log
    {
        // Compara la línea de código contra la lista de patrones
        public bool CompararPatron(string LineaCodigo, List<string> ListaPatrones)
        {
            bool respuesta = false;

            try
            {
                foreach (var patron in ListaPatrones)
                {
                    Regex regex = new Regex(@"" + patron.ToLower() + "");
                    Match match = regex.Match(LineaCodigo.ToLower());

                    if (match.Success)
                    {
                        respuesta = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR CompararPatrones.CompararPatron " + ex.Message.ToString());
            }
            
            return respuesta;
        }

        public int CompararCatalogo(string LineaCodigo, List<ListaObjetosBD> ListaObjetosBD)
        {
            int idObjeto = 0;
            const int esTabla = 1;
            const int esVista = 3;

            try
            {
                foreach (var objetoBD in ListaObjetosBD)
                {
                    Regex regex = new Regex(@"" + objetoBD.NombreObjetoBD.ToLower() + "");
                    Match match = regex.Match(LineaCodigo.ToLower());

                    if (match.Success)
                    {
                        // Para las Tablas y Vistas se hace otra búsqueda de instrucciones para aumentar la posibilidad de que es el objeto y no una variable

                        if (objetoBD.IdTipoObjeto == esTabla)
                        {
                            regex = new Regex(@"(from|insert|delete|update|truncate|drop|alter)");
                            match = regex.Match(LineaCodigo.ToLower());

                            if (match.Success)
                            {
                                idObjeto = objetoBD.IdObjetoBD;
                                break;
                            }
                        }

                        if (objetoBD.IdTipoObjeto == esVista)
                        {
                            regex = new Regex(@"(create|alter|from)");
                            match = regex.Match(LineaCodigo.ToLower());

                            if (match.Success)
                            {
                                idObjeto = objetoBD.IdObjetoBD;
                                break;
                            }
                        }

                        idObjeto = objetoBD.IdObjetoBD;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR CompararPatrones.CompararCatalogo " + ex.Message.ToString());
            }

            return idObjeto;
        }
    }
}
