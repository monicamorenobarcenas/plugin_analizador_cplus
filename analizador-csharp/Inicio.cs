﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using analizador_csharp.Analizadores;
using analizador_csharp.Comunes;
using analizador_csharp.LogFisico;

namespace analizador_csharp
{
    public class Inicio : Log
    {
        public Inicio()
        {

        }

        public bool IniciarAnalisis(int UsuarioID, int AplicacionID, int[] Analizadores, string Ruta, string Cadena)
        {
            
            bool respuesta = false;
            try
            {
                // Recuperar el ProcesoID siguiente 
                int nuevoProceso;
                var administrarProcesos = new AdministrarProcesos();

                nuevoProceso = administrarProcesos.ObtenerProceso(Cadena);

                if (nuevoProceso == 0)
                    throw new Exception();

                // Ejecutar el analizador correspondiente por cada tipo de analizador que es enviado en el arreglo
                foreach (var analizador in Analizadores)
                {
                    switch (analizador)
                    {
                        case 1:
                            var informacionGeneral = new InformacionGeneral();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador, Cadena))
                            {
                                respuesta = informacionGeneral.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso, Cadena);
                                EscribeLog("INFO Información General terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 2:
                            var tecnologias = new Tecnologias();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador, Cadena))
                            {
                                respuesta = tecnologias.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso, Cadena);
                                EscribeLog("INFO Tecnologías terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 3:
                            var insumos = new Insumos();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador, Cadena))
                            {
                                respuesta = insumos.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso, Cadena);
                                EscribeLog("INFO Insumos terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 4:
                            var dependencias = new Dependencias();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador, Cadena))
                            {
                                respuesta = dependencias.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso, Cadena);
                                EscribeLog("INFO Dependencias terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 5:
                            var servidores = new Servidores();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador, Cadena))
                            {
                                respuesta = servidores.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso, Cadena);
                                EscribeLog("INFO Servidores terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 6:
                            var objetosBaseDeDatos = new ObjetosBaseDeDatos();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador, Cadena))
                            {
                                respuesta = objetosBaseDeDatos.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso, Cadena);
                                EscribeLog("INFO Objetos Base de Datos terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 7:
                            var interfaces = new Interfaces();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador, Cadena))
                            {
                                respuesta = interfaces.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso, Cadena);
                                EscribeLog("INFO Interfaces terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 8:
                            var estructura = new Estructura();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador, Cadena))
                            {
                                respuesta = estructura.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso, Cadena);
                                EscribeLog("INFO Estructura terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;
                    }
                }
                
            }

            catch (Exception ex)
            {
                EscribeLog ("ERROR Inicio.IniciarAnalisis " + ex.Message.ToString());
            }

            return respuesta;
        }
    }
}
