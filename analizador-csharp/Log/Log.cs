﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace analizador_csharp.LogFisico
{
    public class Log
    {
        private string rutaLog;
        public string RutaLog
        {
            get { return rutaLog; }
            set { rutaLog = value; }
        }

        private string mensajeExcepcion;
        public string MensajeExcepcion
        {
            get { return mensajeExcepcion; }
            set { mensajeExcepcion = value; }
        }

        private void InicializaRuta()
        {
            try
            {
                rutaLog = Directory.GetCurrentDirectory() + "\\Logs\\";
            }
            catch (Exception)
            {

            }
        }

        public bool EscribeLog(string Mensaje)
        {
            bool respuesta = false;
            try
            {
                InicializaRuta();
                if (!Directory.Exists(rutaLog))
                    Directory.CreateDirectory(rutaLog);
                
                StreamWriter streamWriter = new StreamWriter(rutaLog + "Analizador_CSharp" + DateTime.Now.ToString("yyyyMMdd") + ".log", true);
                streamWriter.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " " + Mensaje);
                streamWriter.Flush();
                streamWriter.Close();
                respuesta = true;
            }
            catch (Exception Err)
            {
                mensajeExcepcion = Err.Message.ToString();
                respuesta = false;
            }
            return respuesta;
        }
    }
}
