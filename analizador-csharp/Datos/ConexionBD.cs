﻿using System;
using System.Data.SqlClient;
using System.Data;
using analizador_csharp.LogFisico;

namespace analizador_csharp
{
    public class ConexionBD : Log
    {
        private SqlConnection CadenaDeConexion;
        private SqlCommand Comando;
        private SqlTransaction TransaccionSQL;

        public ConexionBD()
        {
            
        }
        public bool AbrirConexion(string Cadena)
        {

            try
            {
                CadenaDeConexion = new SqlConnection(Cadena);               
            }
            catch (Exception)
            {

                throw;
            }

            return true;
        }
        public void CerrarConexion()
        {
            try
            {
                if (CadenaDeConexion != null)
                {
                    if (CadenaDeConexion.State != ConnectionState.Closed)
                        CadenaDeConexion.Close();
                }
            }
            catch (SqlException Err)
            {
                EscribeLog("ERROR ConexionBD.CerrarConexion " + Err.Message.ToString() + " "
                                                               + Err.Number.ToString() + " "
                                                               + Err.Procedure.ToString() + " "
                                                               + Err.LineNumber.ToString() + " "
                                                               + Err.State.ToString());
            }
        }

        // --- Transacciones SQL ---

        public bool InicializaTransaccion()
        {
            bool respuesta = false;
            try
            {
                TransaccionSQL = CadenaDeConexion.BeginTransaction("TransaccionSQL");
            }
            catch (Exception Err)
            {
                EscribeLog("ERROR ConexionBD.InicializaTransaccion " + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool CommitTransaccion()
        {
            bool respuesta = false;
            try
            {
                TransaccionSQL.Commit();
            }
            catch (Exception Err)
            {
                EscribeLog("ERROR ConexionBD.CommitTransaccion " + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool RollBackTransaccion()
        {
            bool respuesta = false;
            try
            {
                TransaccionSQL.Rollback();
            }
            catch (Exception Err)
            {
                EscribeLog("ERROR ConexionBD.RollBackTransaccion " + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool AsignaTransaccion()
        {
            bool respuesta = false;
            try
            {
                Comando.Transaction = TransaccionSQL;
            }
            catch (Exception Err)
            {
                EscribeLog("ERROR ConexionBD.AsignaTransaccion " + Err.Message.ToString());
            }
            return respuesta;
        }

        // ---

        // --- Procedimientos Almacenados ---

        public void PreparaStoredProcedure(string SP)
        {
            try
            {
                CadenaDeConexion.Open();
                Comando = new SqlCommand();
                Comando.Connection = CadenaDeConexion;
                Comando.CommandText = SP;
                Comando.CommandType = CommandType.StoredProcedure;
            }
            catch (SqlException Err)
            {
                CerrarConexion();
                EscribeLog("ERROR ConexionBD.EjecutaStoreProcedure " + SP + " " + Err.Message.ToString());
            }
        }

        public void PreparaStoredProcedureMasivo(string SP)
        {
            try
            {
                if (CadenaDeConexion.State == ConnectionState.Broken)
                {
                    CadenaDeConexion.Close();
                    CadenaDeConexion.Open();
                }

                if (CadenaDeConexion.State == ConnectionState.Closed)
                    CadenaDeConexion.Open();

                Comando = new SqlCommand();
                Comando.Connection = CadenaDeConexion;
                Comando.CommandText = SP;
                Comando.CommandType = CommandType.StoredProcedure;

            }
            catch (SqlException Err)
            {
                EscribeLog("ERROR ConexionBD.PrepareSp " + SP + " " + Err.Message.ToString());
            }
        }

        public void EjecutaStoredProcedureNonQuery()
        {
            try
            {
                Comando.ExecuteNonQuery();
            }
            catch (SqlException Err)
            {
                CerrarConexion();
                EscribeLog("ERROR ConexionBD.EjecutaStoreProcedureNonQuery " + Err.Message.ToString()
                                                                          + Err.Number.ToString() + " "
                                                                          + Err.Procedure.ToString() + " "
                                                                          + Err.LineNumber.ToString() + " "
                                                                          + Err.State.ToString());
            }
        }

        public SqlDataReader AlmacenarStoredProcedureDataReader()
        {
            SqlDataReader sqlDataReader = null;
            try
            {
                sqlDataReader = Comando.ExecuteReader();
            }
            catch (SqlException Err)
            {
                CerrarConexion();
                EscribeLog("ERROR ConexionBD.AlmacenarStoredProcedureDataReader " + Err.Message.ToString() + " "
                                                                             + Err.Number.ToString() + " "
                                                                             + Err.Procedure.ToString() + " "
                                                                             + Err.LineNumber.ToString() + " "
                                                                             + Err.State.ToString());
            }
            return sqlDataReader;
        }

        public void CargaParametro(string Parametro, SqlDbType Tipo, int Tamano, ParameterDirection Direccion, object Valor)
        {
            try
            {
                Comando.Parameters.Add(new SqlParameter(Parametro, Tipo, Tamano, Direccion, true, 0, 0, "", DataRowVersion.Current, Valor));
            }
            catch (SqlException Err)
            {
                CerrarConexion();
                EscribeLog("ERROR ConexionBD.CargaParametro " + Err.Message.ToString() + " "
                                                         + Err.Number.ToString() + " "
                                                         + Err.Procedure.ToString() + " "
                                                         + Err.LineNumber.ToString() + " "
                                                         + Err.State.ToString());
            }

        }

        public object RegresaValorParam(string name)
        {
            try
            {
                return Comando.Parameters[name].Value;
            }
            catch (Exception Err)
            {
                EscribeLog("ERROR ConexionBD.RegresaValorParam " + name + " " + Err.Message.ToString());
                return 0;
            }
        }


        // ---
    }
}
