﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data;
using System.Data.SqlClient;

namespace analizador_csharp.Datos
{
    class ObjetosBD : ConexionBD
    {
        private const int catalogoObjetos = 1;
        private const string obtenerArchivos = "sp_libObjetosBD";

        private XmlDocument resultadoXML;
        public XmlDocument ResultadoXML
        {
            get { return resultadoXML; }
        }

        public bool ObtenerCatalogoObjetos(int AplicacionID, string Cadena)
        {
            bool respuesta = false;
            try
            {
                AbrirConexion(Cadena);
                PreparaStoredProcedure(obtenerArchivos);
                CargaParametro("Tipo", SqlDbType.Int, 8, ParameterDirection.Input, catalogoObjetos);
                CargaParametro("IdAplicacion", SqlDbType.Int, 8, ParameterDirection.Input, AplicacionID);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("ObjetosBD");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
                if (respuesta)
                    EscribeLog("INFO ObjetosBD.ObtenerCatalogoObjetos ejecutado exitosamente. | Aplicación: " + AplicacionID);
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR ObjetosBD.ObtenerCatalogoObjetos " + ex.Message.ToString());
            }
            return respuesta;
        }
    }
}
